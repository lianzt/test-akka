import scala.io.Source._

object AnalyzeLog {
  def main(args: Array[String]): Unit = {
    val fn:Option[Any] => Unit = {
      case Some(x) => println(x)
    }
    fn(Option.apply(23))
    val f:Int => String = (x:Int) => x.toString
  }

  private def analyze(): Unit = {
    fromFile("/home/lzt/tmp/lk-pic-upload.log", "utf-8")
      .getLines()
      .filter(_.indexOf("检查文件修改时间") > 0)
      .map(l => (l.substring(6, 25), l.split("\\\\")(2)))
      .toList.groupBy[String](_._1)
      .map(x => (x._1, x._2.length))
      .toList
      .sortBy(x => x._1)
      .foreach(x => println(s"时间：${x._1} 上传照片：${x._2} 张"))
  }
}
