import java.io.File

import akka.actor.{Actor, ActorSystem, Props}
import akka.util.Timeout
import akka.pattern.ask

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.Await
import scala.util.{Failure, Success}

object FutureTest {

  class MyActor extends Actor {
    override def receive: Receive = {
      case x: String => {
        import scala.io.Source._
        val file = new File(x)
        sender() ! fromFile(file, "utf-8").length
      }
      case _ => println("error message")
    }
  }

  def main(args: Array[String]): Unit = {
    val system = ActorSystem("testfuture")
    val myActor = system.actorOf(Props[MyActor])

    implicit val timeout = Timeout(5 seconds)
    val future = myActor ? "/home/lzt/tmp/test.csv"
//    implicit val
    future.onComplete{
      case Success(v) => println(s"onSuccess v=$v")
      case Failure(e) => println(s"onFailure e=${e.getMessage}")
    }
    val result = Await.result(future, timeout.duration).asInstanceOf[Int]
    println(s"result = $result")
  }
}
