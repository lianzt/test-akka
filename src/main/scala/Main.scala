import akka.actor.{Actor, ActorSystem, PoisonPill, Props}
import akka.event.Logging

object Main {

  val props = Props[MyActor]
  val system = ActorSystem("myactor")
  val myActor = system.actorOf(props)
  val wordActor = system.actorOf(Props[WordActor])


  class MyActor extends  Actor {
    val log = Logging(context.system, this)

    override def receive: Receive = {
      case "test" => {
        log.info("received test")
//        self ! PoisonPill
        self ! "test111"
        wordActor ! "test111"
      }
      case _ => log.info("received unknnown message")
    }
  }

  class WordActor extends  Actor {
    val log = Logging(context.system, this)

    override def receive: Receive = {
      case x: String => {
        log.info("receive word {}", x)
      }
    }
  }

  def main(args: Array[String]): Unit = {
    myActor ! "test"
  }
}
